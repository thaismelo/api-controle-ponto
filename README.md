# API PONTO ELETRÔNICO
Esta API de Controle de Ponto Eletrônico foi criada para exercitar todo o 
conhecimento que adquiri no PREREQ - Mastertech.

## USUÁRIO
### Registar Usuario
##### localhost:8080/usuario/
**Você manda:** Um objeto usuário com os campos preenchidos que deseja cadastrar
**E recebe:** O objeto usuário que foi cadastrado 

**Request:**
```json
POST /login HTTP/1.1
Accept: application/json
Content-Type: application/json
Content-Length: xy

{
    "nomeCompleto": "João Pedro",
    "cpf": "577.282.720-04",
    "email": "joao@gmail.com"
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "id": 1,
    "nomeCompleto": "João Pedro",
    "cpf": "577.282.720-04",
    "email": "joao@gmail.com",
    "dataCadastro": "2020-07-06"
}
```

### Alterar Usuário
##### localhost:8080/usuario/1
**Você manda:** Um objeto usuário com os campos preenchidos que deseja alterar
**E recebe:** O objeto usuário que foi alterado 

**Request:**
```json
PUT /login HTTP/1.1
Accept: application/json
Content-Type: application/json
Content-Length: xy

{
    "nomeCompleto": "João Pedro",
    "cpf": "577.282.720-04",
    "email": "joaopedro@gmail.com"
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "id": 1,
    "nomeCompleto": "João Pedro",
    "cpf": "577.282.720-04",
    "email": "joao@gmail.com",
    "dataCadastro": "2020-07-06"
}
```

### Buscar Usuário
##### localhost:8080/usuario/1
**Você manda:** O id do usuário que deseja buscar
**E recebe:** O objeto usuário que foi solicitado 

**Request:**
`GET /usuario/idUsuario`


**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "id": 1,
    "nomeCompleto": "João Pedro",
    "cpf": "577.282.720-04",
    "email": "joao@gmail.com",
    "dataCadastro": "2020-07-06"
}
```

### Buscar Usuário
##### localhost:8080/usuario
**Recebe:** A lista de usuários cadastrados

**Request:**
`GET /usuario`


**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

[
{
        "id": 1,
        "nomeCompleto": "João Pedro",
        "cpf": "577.282.720-04",
        "email": "joaopedro@gmail.com",
        "dataCadastro": "2020-07-06"
    },
    {
        "id": 2,
        "nomeCompleto": "Thais de Melo",
        "cpf": "771.408.930-04",
        "email": "melothaiss@gmail.com",
        "dataCadastro": "2020-07-06"
    }
]
```

## PONTOELETRONICO
### Registar Ponto Eletronico
##### localhost:8080/pontoeletronico
**Você manda:** O id do usuário que deseja registrar o ponto e o tipo de marcação
(ENTRADA/SAIDA)
**E recebe:** O objeto de ponto eletronico que foi registrado 

**Request:**
```json
POST /login HTTP/1.1
Accept: application/json
Content-Type: application/json
Content-Length: xy

{
    "idUsuario": 1,
    "tipoBatidaEnum": "SAIDA"
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "id": 2,
    "usuario": {
        "id": 1,
        "nomeCompleto": "João Pedro",
        "cpf": "577.282.720-04",
        "email": "joaopedro@gmail.com",
        "dataCadastro": "2020-07-06"
    },
    "dataHora": "2020-07-06 18:00:00",
    "tipoBatidaEnum": "SAIDA"
}
```

### Listar Ponto Eletrônico e Soma de Horas
##### localhost:8080/pontoeletronico/1
**Você manda:** O id do usuário que deseja receber a lista  com as batidas do
ponto eletrônico
**Recebe:** A lista de pontos batidos e a soma de horas do dia

**Request:**
`GET /pontoeletronico/1`


**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "listaPontoEletronicoBatido": [
        {
            "id": 2,
            "usuario": {
                "id": 1,
                "nomeCompleto": "João Pedro",
                "cpf": "577.282.720-04",
                "email": "joaopedro@gmail.com",
                "dataCadastro": "2020-07-06"
            },
            "dataHora": "2020-07-06 09:00:00",
            "tipoBatidaEnum": "ENTRADA"
        },
        {
            "id": 3,
            "usuario": {
                "id": 1,
                "nomeCompleto": "João Pedro",
                "cpf": "577.282.720-04",
                "email": "joaopedro@gmail.com",
                "dataCadastro": "2020-07-06"
            },
            "dataHora": "2020-07-06 13:34:00",
            "tipoBatidaEnum": "SAIDA"
        },
        {
            "id": 1,
            "usuario": {
                "id": 1,
                "nomeCompleto": "João Pedro",
                "cpf": "577.282.720-04",
                "email": "joaopedro@gmail.com",
                "dataCadastro": "2020-07-06"
            },
            "dataHora": "2020-07-06 14:34:57",
            "tipoBatidaEnum": "ENTRADA"
        },
        {
            "id": 4,
            "usuario": {
                "id": 1,
                "nomeCompleto": "João Pedro",
                "cpf": "577.282.720-04",
                "email": "joaopedro@gmail.com",
                "dataCadastro": "2020-07-06"
            },
            "dataHora": "2020-07-06 18:00:00",
            "tipoBatidaEnum": "SAIDA"
        }
    ],
    "totalDeHoras": "7:59:3"
}

```