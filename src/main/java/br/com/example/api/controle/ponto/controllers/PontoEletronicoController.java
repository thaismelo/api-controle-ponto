package br.com.example.api.controle.ponto.controllers;

import br.com.example.api.controle.ponto.models.PontoEletronico;
import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoEntradaDTO;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoSaidaDTO;
import br.com.example.api.controle.ponto.services.PontoEletronicoService;
import br.com.example.api.controle.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/pontoeletronico")
public class PontoEletronicoController {

    @Autowired
    private PontoEletronicoService pontoEletronicoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PontoEletronico cadastrarBatidaDePonto(@RequestBody @Valid PontoEletronicoEntradaDTO pontoEletronicoDto) {
        try {
            PontoEletronico pontoEletronicoObjeto = pontoEletronicoService.cadastrarPontoEletronico(pontoEletronicoDto);
            return pontoEletronicoObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.FOUND)
    public PontoEletronicoSaidaDTO listarPontosDoDiaDoUsuario(@PathVariable long idUsuario) {
        try {
            PontoEletronicoSaidaDTO pontoEletronicoSaidaDtoObjeto = pontoEletronicoService
                    .listarPontosDoDiaETotalHorasTrabalhadas(idUsuario);
            return pontoEletronicoSaidaDtoObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
