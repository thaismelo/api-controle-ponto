package br.com.example.api.controle.ponto.models.dtos;

import br.com.example.api.controle.ponto.models.PontoEletronico;

public class PontoEletronicoSaidaDTO {
    private Iterable<PontoEletronico> listaPontoEletronicoBatido;

    private String totalDeHoras;

    public PontoEletronicoSaidaDTO(Iterable<PontoEletronico> pontoEletronicoList, String totalDeHoras) {
        this.listaPontoEletronicoBatido = pontoEletronicoList;
        this.totalDeHoras = totalDeHoras;
    }

    public Iterable<PontoEletronico> getListaPontoEletronicoBatido() {
        return listaPontoEletronicoBatido;
    }

    public void setListaPontoEletronicoBatido(Iterable<PontoEletronico> listaPontoEletronicoBatido) {
        this.listaPontoEletronicoBatido = listaPontoEletronicoBatido;
    }

    public String getTotalDeHoras() {
        return totalDeHoras;
    }

    public void setTotalDeHoras(String totalDeHoras) {
        this.totalDeHoras = totalDeHoras;
    }
}
