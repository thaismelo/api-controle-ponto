package br.com.example.api.controle.ponto.repositories;

import br.com.example.api.controle.ponto.enums.TipoBatidaEnum;
import br.com.example.api.controle.ponto.models.PontoEletronico;
import br.com.example.api.controle.ponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface PontoEletronicoRepository extends CrudRepository<PontoEletronico, Long> {
    Iterable<PontoEletronico> findAllByUsuarioOrderByDataHoraAsc(Usuario usuario);
    Iterable<PontoEletronico> findAllByUsuarioAndTipoBatidaEnumOrderByDataHoraAsc(Usuario usuario,
                                                                              TipoBatidaEnum tipoBatida);

}
