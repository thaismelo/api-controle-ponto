package br.com.example.api.controle.ponto.models.dtos;

import br.com.example.api.controle.ponto.enums.TipoBatidaEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PontoEletronicoEntradaDTO {
    private long idUsuario;

    private TipoBatidaEnum tipoBatidaEnum;

    public PontoEletronicoEntradaDTO() {
    }

    public PontoEletronicoEntradaDTO(@NotBlank(message = "É obrigatório informar o id do usuário")
                                     @NotNull(message = "É obrigatório informar o id do usuário")
                                             long idUsuario,
                                     @NotBlank(message = "É obrigatório informar o tipo de batida (ENTRADA/SAIDA)")
                                     @NotNull(message = "É obrigatório informar o tipo de batida (ENTRADA/SAIDA)")
                                             TipoBatidaEnum tipoBatidaEnum) {
        this.idUsuario = idUsuario;
        this.tipoBatidaEnum = tipoBatidaEnum;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public TipoBatidaEnum getTipoBatidaEnum() {
        return tipoBatidaEnum;
    }

    public void setTipoBatidaEnum(TipoBatidaEnum tipoBatidaEnum) {
        this.tipoBatidaEnum = tipoBatidaEnum;
    }
}
