package br.com.example.api.controle.ponto.repositories;

import br.com.example.api.controle.ponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
}
