package br.com.example.api.controle.ponto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiControlePontoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiControlePontoApplication.class, args);
	}

}
