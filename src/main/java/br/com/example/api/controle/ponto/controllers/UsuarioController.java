package br.com.example.api.controle.ponto.controllers;

import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registarUsuario(@RequestBody @Valid Usuario usuario) {
        try {
            Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);
            return usuarioObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.OK)
    public Usuario atualizarUsuario(@PathVariable long idUsuario, @RequestBody @Valid Usuario usuario) {
        try {
            Usuario usuarioObjeto = usuarioService.atualizarUsuario(idUsuario, usuario);
            return usuarioObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.FOUND)
    public Usuario buscarUsuarioPorId(@PathVariable long idUsuario) {
        try {
            Usuario usuarioObjeto = usuarioService.buscarPorId(idUsuario);
            return usuarioObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @GetMapping
    @ResponseStatus(HttpStatus.FOUND)
    public Iterable<Usuario> buscarTodosUsuarios() {
        try {
            Iterable<Usuario> usuarios = usuarioService.buscarTodosOsUsuarios();
            return usuarios;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
