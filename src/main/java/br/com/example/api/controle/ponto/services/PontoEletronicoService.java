package br.com.example.api.controle.ponto.services;

import br.com.example.api.controle.ponto.enums.TipoBatidaEnum;
import br.com.example.api.controle.ponto.models.PontoEletronico;
import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoEntradaDTO;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoSaidaDTO;
import br.com.example.api.controle.ponto.repositories.PontoEletronicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class PontoEletronicoService {

    @Autowired
    private PontoEletronicoRepository pontoEletronicoRepository;

    @Autowired
    private UsuarioService usuarioService;

    public PontoEletronico cadastrarPontoEletronico(PontoEletronicoEntradaDTO pontoEletronicoDto) {
        Usuario usuario = usuarioService.buscarPorId(pontoEletronicoDto.getIdUsuario());

        if (this.consultarPontosDoDiaPorUsuario(usuario, pontoEletronicoDto.getTipoBatidaEnum())) {
            PontoEletronico pontoEletronicoObjeto = new PontoEletronico(usuario, LocalDateTime.now(),
                    pontoEletronicoDto.getTipoBatidaEnum());

            pontoEletronicoRepository.save(pontoEletronicoObjeto);

            return pontoEletronicoObjeto;
        }
        throw new RuntimeException("Não foi possível cadastrar batida de ponto");
    }

    public boolean consultarPontosDoDiaPorUsuario(Usuario usuario, TipoBatidaEnum tipoBatidaEnum) {
        Iterable<PontoEletronico> pontoEletronicoUsuarioList = this.listarPontoEletronicoDoDiaPorUsuario(usuario);

        if (pontoEletronicoUsuarioList.iterator().hasNext()) {
            List<PontoEletronico> pontoList = ((List<PontoEletronico>) pontoEletronicoUsuarioList);

            if (pontoList.get(pontoList.size() - 1).getTipoBatidaEnum() != tipoBatidaEnum) {
                return true;
            }
            throw new RuntimeException("O usuário já cadastrou batida de " + tipoBatidaEnum);
        } else {
            if (tipoBatidaEnum == TipoBatidaEnum.ENTRADA) {
                return true;
            }
            throw new RuntimeException("O usuário não pode cadastrar " + tipoBatidaEnum +
                    " pois ainda não bateu o ponto de entrada");
        }
    }

    public Iterable<PontoEletronico> listarPontoEletronicoDoDiaPorUsuario(Usuario usuario) {
        Iterable<PontoEletronico> pontoEletronicoList = pontoEletronicoRepository
                .findAllByUsuarioOrderByDataHoraAsc(usuario);
        return pontoEletronicoList;
    }

    public Iterable<PontoEletronico> listarPontoEntrada(Usuario usuario) {
        Iterable<PontoEletronico> iterableBatidaEntrada = pontoEletronicoRepository
                .findAllByUsuarioAndTipoBatidaEnumOrderByDataHoraAsc(usuario, TipoBatidaEnum.ENTRADA);
        return iterableBatidaEntrada;
    }

    public Iterable<PontoEletronico> listarPontoSaida(Usuario usuario) {
        Iterable<PontoEletronico> iterableBatidaSaida = pontoEletronicoRepository
                .findAllByUsuarioAndTipoBatidaEnumOrderByDataHoraAsc(usuario, TipoBatidaEnum.SAIDA);
        return iterableBatidaSaida;
    }

    public PontoEletronicoSaidaDTO listarPontosDoDiaETotalHorasTrabalhadas(long idUsuario) {
        Usuario usuario = usuarioService.buscarPorId(idUsuario);

        Iterable<PontoEletronico> pontoEletronicoUsuarioList =
                this.listarPontoEletronicoDoDiaPorUsuario(usuario);

        if (pontoEletronicoUsuarioList.iterator().hasNext()) {
            PontoEletronicoSaidaDTO pontoEletronicoSaidaDTO = new PontoEletronicoSaidaDTO(pontoEletronicoUsuarioList,
                    this.calcularTotalDeHorasTrabalhadas(usuario));

            return pontoEletronicoSaidaDTO;
        }
        throw new RuntimeException("O usuário ainda não possui ponto do dia");
    }

    public String calcularTotalDeHorasTrabalhadas(Usuario usuario) {
        long totalHora = 0;
        long totalMin = 0;
        long totalSeg = 0;

        List<PontoEletronico> listaPontoEntrada = ((List<PontoEletronico>) this.listarPontoEntrada(usuario));
        List<PontoEletronico> listaPontoSaida = ((List<PontoEletronico>) this.listarPontoSaida(usuario));

        for (int i = 0; listaPontoEntrada.size() > i && listaPontoSaida.size() > i; i++) {
            Duration duration = Duration.between(listaPontoEntrada.get(i).getDataHora(),
                    listaPontoSaida.get(i).getDataHora());
            totalHora += duration.toHours();
            totalMin += duration.toMinutes();
            totalSeg += duration.getSeconds();
        }

        return totalHora + ":" + totalMin % 60 + ":" + totalSeg % 60;
    }
}
