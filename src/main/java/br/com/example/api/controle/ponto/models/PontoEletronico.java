package br.com.example.api.controle.ponto.models;

import br.com.example.api.controle.ponto.enums.TipoBatidaEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class PontoEletronico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long id;

    @OneToOne
    private Usuario usuario;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDateTime dataHora;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "É obrigatório informar o tipo da batida (entrada ou saida)")
    private TipoBatidaEnum tipoBatidaEnum;


    public PontoEletronico() {
    }

    public PontoEletronico(Usuario usuario, LocalDateTime dataHora,
                           @NotNull(message = "É obrigatório informar o tipo da batida (entrada ou saida)")
                                   TipoBatidaEnum tipoBatidaEnum) {
        this.usuario = usuario;
        this.dataHora = dataHora;
        this.tipoBatidaEnum = tipoBatidaEnum;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }

    public TipoBatidaEnum getTipoBatidaEnum() {
        return tipoBatidaEnum;
    }

    public void setTipoBatidaEnum(TipoBatidaEnum tipoBatidaEnum) {
        this.tipoBatidaEnum = tipoBatidaEnum;
    }
}
