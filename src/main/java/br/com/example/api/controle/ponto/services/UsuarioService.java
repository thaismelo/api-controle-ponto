package br.com.example.api.controle.ponto.services;

import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){
        usuario.setDataCadastro(LocalDate.now());
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Usuario buscarPorId(long id){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if (optionalUsuario.isPresent()){

            return optionalUsuario.get();
        }
        throw new RuntimeException("O usuário não foi encontrado");
    }

    public Iterable<Usuario> buscarTodosOsUsuarios(){
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }

    public Usuario atualizarUsuario(long id, Usuario usuario){
        if (usuarioRepository.existsById(id)){
            usuario.setId(id);
            Usuario usuarioObjeto = usuarioRepository.save(usuario);

            return usuarioObjeto;
        }
        throw new RuntimeException("O usuário não foi encontrado: " + id);
    }

}
