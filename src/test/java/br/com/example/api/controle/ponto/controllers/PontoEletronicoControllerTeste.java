package br.com.example.api.controle.ponto.controllers;

import br.com.example.api.controle.ponto.enums.TipoBatidaEnum;
import br.com.example.api.controle.ponto.models.PontoEletronico;
import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoEntradaDTO;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoSaidaDTO;
import br.com.example.api.controle.ponto.services.PontoEletronicoService;
import br.com.example.api.controle.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(PontoEletronicoController.class)
public class PontoEletronicoControllerTeste {

    @MockBean
    private PontoEletronicoService pontoEletronicoService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    PontoEletronico pontoEletronico;
    PontoEletronicoEntradaDTO pontoEletronicoEntradaDTO;
    PontoEletronicoSaidaDTO pontoEletronicoSaidaDTO;
    List<PontoEletronico> listaPontos;


    LocalDateTime dateTime;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Thais Melo");
        usuario.setCpf("771.408.930-04");
        usuario.setEmail("thais@gmail.com");

        pontoEletronico = new PontoEletronico();
        pontoEletronico.setId(1);
        pontoEletronico.setUsuario(usuario);
        pontoEletronico.setTipoBatidaEnum(TipoBatidaEnum.ENTRADA);

        pontoEletronicoEntradaDTO = new PontoEletronicoEntradaDTO();
        pontoEletronicoEntradaDTO.setTipoBatidaEnum(TipoBatidaEnum.ENTRADA);

        listaPontos = new ArrayList<>();
        listaPontos.add(pontoEletronico);

        pontoEletronicoSaidaDTO = new PontoEletronicoSaidaDTO(listaPontos, "8:12:0");

        dateTime = LocalDateTime.now();
    }


    @Test
    public void testarCadastrarPontoEletronico() throws Exception {

        Mockito.when(pontoEletronicoService.cadastrarPontoEletronico(Mockito.any(PontoEletronicoEntradaDTO.class)))
                .then(pontoEletronicoObjeto -> {
                    pontoEletronico.setId(1);
                    pontoEletronico.setDataHora(dateTime);
            return pontoEletronico;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto = mapper.writeValueAsString(pontoEletronico);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/pontoeletronico")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarCadastrarPontoEletronicoNegativo() throws Exception {

        Mockito.when(pontoEletronicoService.cadastrarPontoEletronico(Mockito.any(PontoEletronicoEntradaDTO.class)))
                .thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto= mapper.writeValueAsString(pontoEletronico);

        mockMvc.perform(MockMvcRequestBuilders.post("/pontoeletronico")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarListarPontosDoDiaDoUsuario() throws Exception {
        usuario.setId(1);
        Mockito.when(pontoEletronicoService.listarPontosDoDiaETotalHorasTrabalhadas(Mockito.anyLong()))
                .thenReturn(pontoEletronicoSaidaDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/pontoeletronico/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }

    @Test
    public void testarListarPontosDoDiaDoUsuarioNegativo() throws Exception {
        Mockito.when(pontoEletronicoService.listarPontosDoDiaETotalHorasTrabalhadas(Mockito.anyLong()))
                .thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/pontoeletronico/33")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

}
