package br.com.example.api.controle.ponto.controllers;

import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTeste {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setNomeCompleto("Thais Melo");
        usuario.setCpf("771.408.930-04");
        usuario.setEmail("melothaiss@gmail.com");
    }

    @Test
    public void tesstarBuscarTodosUsuarios() throws Exception {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioService.buscarTodosOsUsuarios()).thenReturn(usuarios);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarBuscarUsuarioPorId() throws Exception {
        usuario.setId(33);
        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenReturn(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(33)));
    }

    @Test
    public void testarBuscarUsuarioPorIdNegativo() throws Exception {
        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/50")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testarRegistarUsuario() throws Exception {
        Mockito.when(usuarioService.salvarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
            usuario.setId(1);
            usuario.setDataCadastro(LocalDate.now());
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo((LocalDate.now().toString()))));
    }

    @Test
    public void testarAtualizarUsuario() throws Exception {
        usuario.setEmail("thais@gmail.com");
        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyLong(), Mockito.any(Usuario.class)))
                .thenReturn(usuario);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("thais@gmail.com"));
    }

    @Test
    public void testarAtualizarUsuarioNegativo() throws Exception {
        usuario.setId(1);
        usuario.setDataCadastro(LocalDate.now());
        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyInt(), Mockito.any(Usuario.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/102")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
