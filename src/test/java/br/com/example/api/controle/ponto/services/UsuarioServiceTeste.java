package br.com.example.api.controle.ponto.services;

import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTeste {
    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Thais Melo");
        usuario.setCpf("771.408.930-04");
        usuario.setEmail("melothaiss@gmail.com");
        usuario.setDataCadastro(LocalDate.now());
    }

    @Test
    public void testarBuscarPorTodosOsLeads() {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuarioIterable = usuarioService.buscarTodosOsUsuarios();

        Assertions.assertEquals(usuarios, usuarioIterable);
    }

    @Test
    public void testarbuscarPorId(){
        Optional<Usuario> optionalUsuario =  Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optionalUsuario);

        Usuario usuarioObjeto = usuarioService.buscarPorId(Mockito.anyLong());

        Assertions.assertEquals(usuarioObjeto, optionalUsuario.get());

    }

    @Test
    public void testarbuscarPorIdNegativo(){
        Optional<Usuario> optionalUsuario =  Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.buscarPorId(Mockito.anyLong());});
    }

    @Test
    public void testarAtualizarUsuarioComSucesso(){
        boolean existeUsuario = true;
        Mockito.when(usuarioRepository.existsById(Mockito.anyLong())).thenReturn(existeUsuario);

        usuario.setId(1);

        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        Usuario usuarioObjeto = usuarioService.atualizarUsuario(1, usuario);

        Assertions.assertSame(usuario, usuarioObjeto);
    }

    @Test
    public void testarAtualizarUsuarioNegativo(){
        boolean existeUsuario = false;
        Mockito.when(usuarioRepository.existsById(Mockito.anyLong())).thenReturn(existeUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.atualizarUsuario(Mockito.anyInt(), usuario);});
    }



}
