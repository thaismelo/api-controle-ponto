package br.com.example.api.controle.ponto.services;

import br.com.example.api.controle.ponto.enums.TipoBatidaEnum;
import br.com.example.api.controle.ponto.models.PontoEletronico;
import br.com.example.api.controle.ponto.models.Usuario;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoEntradaDTO;
import br.com.example.api.controle.ponto.models.dtos.PontoEletronicoSaidaDTO;
import br.com.example.api.controle.ponto.repositories.PontoEletronicoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class PontoEletronicoServiceTeste {
    @MockBean
    private PontoEletronicoRepository pontoEletronicoRepository;

    @Autowired
    private PontoEletronicoService pontoEletronicoService;

    @MockBean
    UsuarioService usuarioService;

    Usuario usuario;
    PontoEletronico pontoEletronico;
    PontoEletronicoEntradaDTO pontoEletronicoEntradaDTO;
    PontoEletronicoSaidaDTO pontoEletronicoSaidaDTO;
    List<PontoEletronico> listaPontos;
    Iterable<PontoEletronico> iterablePontos;

    LocalDateTime dateTime;

    @BeforeEach
    public void setUp(){

        dateTime = LocalDateTime.now();

        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Thais Melo");
        usuario.setCpf("771.408.930-04");
        usuario.setEmail("thais@gmail.com");

        pontoEletronico = new PontoEletronico();
        pontoEletronico.setId(1);
        pontoEletronico.setUsuario(usuario);
        pontoEletronico.setTipoBatidaEnum(TipoBatidaEnum.ENTRADA);
        pontoEletronico.setDataHora(dateTime);

        pontoEletronicoEntradaDTO = new PontoEletronicoEntradaDTO();
        pontoEletronicoEntradaDTO.setTipoBatidaEnum(TipoBatidaEnum.ENTRADA);

        listaPontos = new ArrayList<>();
        listaPontos.add(pontoEletronico);

        pontoEletronicoSaidaDTO = new PontoEletronicoSaidaDTO(listaPontos, "8:12:0");
    }

    @Test
    public void testarCadastrarPontoEletronico(){
        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenReturn(usuario);

        PontoEletronico pontoEletronicoObjeto = pontoEletronicoService
                .cadastrarPontoEletronico(pontoEletronicoEntradaDTO);

        Assertions.assertEquals(pontoEletronicoObjeto, pontoEletronicoObjeto);
    }

    @Test
    public void testarCadastrarPontoEletronicoNegativo(){
        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenThrow(RuntimeException.class);

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoEletronicoService.cadastrarPontoEletronico(pontoEletronicoEntradaDTO);
        });
    }

    @Test
    public void testarConsultarPontosDoDiaPorUsuarioListaVaziaSemEntrada(){
        Mockito.when(pontoEletronicoRepository.findAllByUsuarioOrderByDataHoraAsc(Mockito.any(Usuario.class)))
                .thenReturn(iterablePontos);

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoEletronicoService.consultarPontosDoDiaPorUsuario(usuario, TipoBatidaEnum.SAIDA);
        });
    }

    @Test
    public void testarConsultarPontosDoDiaPorUsuarioListaVaziaTipoEntrada(){
        Mockito.when(pontoEletronicoRepository.findAllByUsuarioOrderByDataHoraAsc(Mockito.any(Usuario.class)))
                .thenReturn(iterablePontos);

        boolean pontoEletronicoObjeto = pontoEletronicoService
                .consultarPontosDoDiaPorUsuario(Mockito.any(Usuario.class), TipoBatidaEnum.ENTRADA);

        Assertions.assertTrue(pontoEletronicoObjeto);
    }

    @Test
    public void testarConsultarPontosDoDiaPorUsuarioComListaETiposIguais(){
        iterablePontos = Arrays.asList(pontoEletronico);

        Mockito.when(pontoEletronicoRepository.findAllByUsuarioOrderByDataHoraAsc(Mockito.any(Usuario.class)))
                .thenReturn(iterablePontos);

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoEletronicoService.consultarPontosDoDiaPorUsuario(usuario, TipoBatidaEnum.ENTRADA);
        });
    }

    @Test
    public void testarConsultarPontosDoDiaPorUsuarioComListaETiposDiferentes(){
        iterablePontos = Arrays.asList(pontoEletronico);

        Mockito.when(pontoEletronicoRepository.findAllByUsuarioOrderByDataHoraAsc(Mockito.any(Usuario.class)))
                .thenReturn(iterablePontos);

        boolean pontoEletronicoObjeto = pontoEletronicoService
                .consultarPontosDoDiaPorUsuario(usuario, TipoBatidaEnum.SAIDA);

        Assertions.assertTrue(pontoEletronicoObjeto);
    }

    @Test
    public void testarListarPontosDoDiaETotalHorasTrabalhadasNegativo(){
        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenReturn(usuario);

        Mockito.when(pontoEletronicoRepository.findAllByUsuarioOrderByDataHoraAsc(Mockito.any(Usuario.class)))
                .thenReturn(iterablePontos);

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoEletronicoService.listarPontosDoDiaETotalHorasTrabalhadas(1);
        });
    }

    @Test
    public void testarListarPontosDoDiaETotalHorasTrabalhadas(){
        iterablePontos = Arrays.asList(pontoEletronico);

        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenReturn(usuario);

        Mockito.when(pontoEletronicoRepository.findAllByUsuarioOrderByDataHoraAsc(Mockito.any(Usuario.class)))
                .thenReturn(iterablePontos);

        PontoEletronicoSaidaDTO pontoEletronicoSaidaDtoObjeto = pontoEletronicoService
                .listarPontosDoDiaETotalHorasTrabalhadas(Mockito.anyLong());

        Assertions.assertEquals(pontoEletronicoSaidaDtoObjeto, pontoEletronicoSaidaDtoObjeto);

        Assertions.assertEquals(((List)iterablePontos).size(),
                ((List)pontoEletronicoSaidaDtoObjeto.getListaPontoEletronicoBatido()).size());

    }

//    @Test
//    public void testarCalcularTotalDeHorasTrabalhadas(){
//
//        pontoEletronico.setDataHora(LocalDate.now().atTime(8, 0));
//        iterablePontos = Arrays.asList(pontoEletronico);
//
//        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenReturn(usuario);
//
//        Mockito.when(pontoEletronicoService.listarPontoEntrada(Mockito.any(Usuario.class))).thenReturn(iterablePontos);

//        pontoEletronico = new PontoEletronico();
//        pontoEletronico.setId(2);
//        pontoEletronico.setUsuario(usuario);
//        pontoEletronico.setDataHora(LocalDate.now().atTime(12, 0));
//        pontoEletronico.setTipoBatidaEnum(TipoBatidaEnum.SAIDA);
//
//        iterablePontos = Arrays.asList(pontoEletronico);
//
//        Mockito.when(pontoEletronicoService.listarPontoSaida(Mockito.any(Usuario.class))).thenReturn(iterablePontos);
//
//        String stringCalculo = pontoEletronicoService
//                .calcularTotalDeHorasTrabalhadas(usuario);
//
//        Assertions.assertSame("4:00:0", stringCalculo);
//
//    }

}
